==================
*TEXT* data reader
==================

*galactic-io-data-text* is a
*TEXT*  data reader plugin
for **GALACTIC**.

The file extension is ``.txt``. There is a first section describing the
observations and the attributes using the keywords ``Observations`` and
``Attributes``, then each individual is described by a space separated list
of attributes. For example:

.. code-block:: text
    :class: admonition

    Observations: 1 2 3 4
    Attributes: a b c d e
    1: a c
    2: a b
    3: b d e
    4: c e
