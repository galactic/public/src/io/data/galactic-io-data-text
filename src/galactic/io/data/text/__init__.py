"""
Text Data reader.
"""

from ._main import TextDataReader, register

__all__ = ("TextDataReader", "register")
