"""
Text  Data reader.
"""

from collections import OrderedDict
from collections.abc import Iterator, Mapping
from typing import TextIO, cast

from galactic.helpers.core import default_repr
from galactic.io.data.core import PopulationFactory


# pylint: disable=too-few-public-methods
class TextDataReader:
    # noinspection PyUnresolvedReferences
    """
    `̀ TXT`̀  Data reader.

    Example
    -------
    >>> from pprint import pprint
    >>> from galactic.io.data.text import TextDataReader
    >>> reader = TextDataReader()
    >>> import io
    >>> data = '''Observations: 1 2 3
    ... Attributes: a b c d e
    ... 1: a c
    ... 2: a b
    ... 3: b d e
    ... '''
    >>> individuals = reader.read(io.StringIO(data))
    >>> pprint({key: sorted(list(value)) for key, value in individuals.items()})
    {'1': ['a', 'c'], '2': ['a', 'b'], '3': ['b', 'd', 'e']}

    """

    __slots__ = ()

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    @classmethod
    def read(cls, data_file: TextIO) -> Mapping[str, object]:
        """
        Read a ``TXT`` data file.

        Parameters
        ----------
        data_file
            A readable text file.

        Returns
        -------
        Mapping[str, object]
            The data.

        Raises
        ------
        ValueError
            If the format is incorrect.

        """
        individuals = OrderedDict()
        observation = data_file.readline().strip("\n \r\t").split(" ")
        if observation[0] != "Observations:":
            raise ValueError("file format is incorrect: Observations missing")
        observation.remove(observation[0])
        attributes = data_file.readline().strip("\n \r\t").split(" ")
        if attributes[0] != "Attributes:":
            raise ValueError("file format is incorrect: Attributes missing")
        attributes.remove(attributes[0])
        line = data_file.readline()[:-1]
        while line:
            individual = line.strip("\n \r\t").split(" ")
            individual[0] = individual[0].replace(":", "")
            if individual[0] in observation:
                line = data_file.readline()
            else:
                raise ValueError("individual not in observations list")
            population = set()
            for i in range(1, len(individual)):
                if individual[i] in attributes:
                    population.add(individual[i])
                else:
                    raise ValueError("attribute not in attributes list")
            individuals[individual[0]] = frozenset(population)
        return individuals

    @classmethod
    def extensions(cls) -> Iterator[str]:
        """
        Get an iterator over the supported extensions.

        Returns
        -------
        Iterator[str]
            An iterator over the supported extensions

        """
        return iter([".txt"])


def register() -> None:
    """
    Register an instance of a ``txt`` data reader.
    """
    PopulationFactory.register_reader(TextDataReader())
