"""Reader text test module."""

from pathlib import Path
from unittest import TestCase

from galactic.io.data.core import PopulationFactory


class TextDataReaderTest(TestCase):
    def test_get_reader(self):
        pathname = Path(__file__).parent / "test.txt"
        with pathname.open(encoding="utf-8") as data_file:
            population = PopulationFactory.create(data_file)
        self.assertEqual(
            population,
            {
                "1": frozenset(["a", "c"]),
                "2": frozenset(["a", "b"]),
                "3": frozenset(["b", "d", "e"]),
                "4": frozenset(["c", "e"]),
            },
        )
