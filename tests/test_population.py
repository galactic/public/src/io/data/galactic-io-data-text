"""Population text test module."""

from unittest import TestCase

from galactic.io.data.core import PopulationFactory
from galactic.io.data.text import TextDataReader


class PopulationTest(TestCase):
    def test_population(self):
        self.assertTrue(
            any(
                isinstance(reader, TextDataReader)
                for reader in PopulationFactory.readers()
            ),
        )
